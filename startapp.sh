echo -e “\n\nStep 1 — Install Java and Git\n\n”

sudo apt update
sudo apt -y install default-jdk git

echo -e “\n\nStep 2 — Install Maven\n\n”

sudo apt search maven 
sudo apt -y install maven
mvn -version
ls -lsa /usr/share/maven
ls -lsa /etc/maven

echo -e “\n\nStep 3 — Install MySql DATABASE\n\n”

sudo apt -y install mysql-server
sudo mysql --version
echo -e “\n\nCreating DATABASE and USER with all privileges to this DATABASE\n\n”
sudo mysql -u root  -e "
CREATE DATABASE SALESMANAGER;
	CREATE USER shopizer IDENTIFIED BY 'shopizer';
	GRANT ALL ON SALESMANAGER.* TO shopizer;
	FLUSH PRIVILEGES;
"

echo -e “\n\nStep 4 — Create Tomcat User\n\n”

sudo groupadd tomcat
sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

cd /tmp
curl -O https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.55/bin/apache-tomcat-9.0.55.tar.gz

#Creating the Tomcat home directory and extracting the its files inside it
sudo mkdir /opt/tomcat
sudo tar xzvf apache-tomcat-*tar.gz -C /opt/tomcat --strip-components=1

# Changing the files and directories permission according to the Tomcat user and group
cd /opt/tomcat
sudo chgrp -R tomcat /opt/tomcat
sudo chmod -R g+r conf
sudo chmod g+x conf
sudo chown -R tomcat webapps/ work/ temp/ logs/

sudo update-java-alternatives -l
JAVA_HOME=`sudo update-java-alternatives -l | awk '{print $3}'`
export JAVA_HOME

cat << EOF > ./tomcat.service
[Unit]
Description=Apache Tomcat Web Application Container
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target

EOF

sudo cp -rf ./tomcat.service /etc/systemd/system/

#Update the daemon and starting the tomcat service
sudo systemctl daemon-reload
sudo systemctl start tomcat
#sudo systemctl status tomcat
sudo ufw allow 8080
sudo systemctl enable tomcat



echo -e “\n\n Step 5 Deploy SHOP Application \n\n”

#Cloning the project from SCM and building the artifact using Maven
#Increase the heap size 
export JAVA_OPTS="-Xms1024m -Xmx1024m -XX:MaxPermSize=256m"
cd 
git clone https://gitlab.com/Rakesh7295/shopizer_legacyapp.git
cd shopizer_legacyapp
bash mvnw clean install
sudo cp -rf sm-shop/target/shopapp.war /opt/tomcat/webapps/

#Restart the Tomcat Service 
sudo systemctl stop tomcat.service
sudo systemctl start tomcat.service

